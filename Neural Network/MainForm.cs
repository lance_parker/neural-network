﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Neural_Network
{
    public partial class MainForm : Form
    {
        private bool _shouldDraw;
        private PictureBox[] _pictureBoxes;
        private double[] _pixels;
        private NeuralNet _neuralNet;

        public MainForm()
        {
            InitializeComponent();
            _shouldDraw = false;
            _pictureBoxes = new PictureBox[9*8]
                                {
                                    pbox00,pbox10,pbox20,pbox30,pbox40,pbox50,pbox60,pbox70,
                                    pbox01,pbox11,pbox21,pbox31,pbox41,pbox51,pbox61,pbox71,
                                    pbox02,pbox12,pbox22,pbox32,pbox42,pbox52,pbox62,pbox72,
                                    pbox03,pbox13,pbox23,pbox33,pbox43,pbox53,pbox63,pbox73,
                                    pbox04,pbox14,pbox24,pbox34,pbox44,pbox54,pbox64,pbox74,
                                    pbox05,pbox15,pbox25,pbox35,pbox45,pbox55,pbox65,pbox75,
                                    pbox06,pbox16,pbox26,pbox36,pbox46,pbox56,pbox66,pbox76,
                                    pbox07,pbox17,pbox27,pbox37,pbox47,pbox57,pbox67,pbox77,
                                    pbox08,pbox18,pbox28,pbox38,pbox48,pbox58,pbox68,pbox78
                                };
            _pixels = new double[9*8];
            _neuralNet = new NeuralNet();
        }

        private void MouseEnterImageBox(object sender, EventArgs e)
        {
            if (_shouldDraw)
            {
                ((PictureBox) sender).BackColor = Color.Black;
            }
        }

        private void ClearButtonClick(object sender, EventArgs e)
        {
            foreach (var control in Controls)
            {
                var pictureBox = control as PictureBox;
                if (pictureBox != null)
                {
                    pictureBox.BackColor = Color.White;
                }
            }
            _shouldDraw = false;
        }

        private void MouseClickAnywhere(object sender, EventArgs e)
        {
            _shouldDraw = !_shouldDraw;
        }

        private void MouseClickAnywhere(object sender, MouseEventArgs e)
        {
            var pictureBox = sender as PictureBox;
            if (pictureBox != null)
            {
                _shouldDraw = !_shouldDraw;
                pictureBox.BackColor = Color.Black;
            }
        }

        private void btnReadLetter_Click(object sender, EventArgs e)
        {
            var input = new double[9*8];
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    input[j*9+i] = _pictureBoxes[j*9 + i].BackColor == Color.Black ? 1.0 : 0.0;
                }
            }

            var result = _neuralNet.TestInput(ref input);

            MessageBox.Show(_neuralNet.InterperetResults(result));
        }

        private void btnTrain_Click(object sender, EventArgs e)
        {
            var hud = new ProgressHUD();
            const int iterations = 50000;
            hud.Show();
            var rand = new Random();
            var trainingData = ReadTrainingData();
            //int consecutiveErrorIncreases = 0;
            //double lowestError = double.PositiveInfinity;
            for (int i = 0; i < iterations; i++)
            {
                trainingData = trainingData.Shuffle();
                //bool allWereCorrect = true;
                //double totalError = 0.0;
                for (int j = 0; j < 10; j++)
                {
                    foreach (KeyValuePair<int, double[][]> keyValuePair in trainingData)
                    {
                        double[] inputLetterData = keyValuePair.Value[j];//[rand.Next(10)];
                        /*double[] results = */_neuralNet.Input(inputLetterData, keyValuePair.Key);
                        //if (_neuralNet.HighestValue(results) != keyValuePair.Key)
                        //    allWereCorrect = false;
                        //totalError += Math.Abs(_neuralNet.SumError);
                    }
                    hud.SetProgress((i*100)/iterations);
                }
                //if (totalError > lowestError)//allWereCorrect)
                //{
                //    if (consecutiveErrorIncreases > 10 && i > 1000)
                //    {
                //        MessageBox.Show("Stopped on iteration: " + i + "\nError term: " + _neuralNet.SumError);
                //        break;
                //    }
                //    lowestError = totalError;
                //    consecutiveErrorIncreases++;
                //}
                //else
                //{
                //    lowestError = totalError;
                //    consecutiveErrorIncreases = 0;
                //}
            }
            hud.SetProgress(100);
            hud.Close();
        }

        private Dictionary<int, double[][]> ReadTrainingData()
        {
            var trainingData = new Dictionary<int, double[][]>();
            if (!File.Exists("train.txt"))
            {
                MessageBox.Show("train.txt does not exist, cannot train");
            }
            else
            {
                using (var reader = new StreamReader("train.txt"))
                {
                    int currentArray = 0;
                    int currentArrayIndex = 0;
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        if (!string.IsNullOrEmpty(line))
                        {
                            if (line.Length > 1)
                            {
                                double[] data = Array.ConvertAll(line.ToArray(), input => double.Parse(input.ToString()));
                                trainingData[currentArray][currentArrayIndex++] = data;
                            }
                            else
                            {
                                int val = int.Parse(line);
                                var arr = new double[10][];
                                trainingData[val] = arr;
                                currentArray = val;
                                currentArrayIndex = 0;
                            }
                        }
                    }
                }
            }
            return trainingData;
        }

        private void saveWeightsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var serializer = new BinaryFormatter();
            var dialog = new SaveFileDialog {DefaultExt = "dat", FileName = "weights.dat", AddExtension = true};
            dialog.ShowDialog();

            using (Stream stream = File.Open(dialog.FileName, FileMode.Create))
            {
                serializer.Serialize(stream, _neuralNet);
            }
        }

        private void loadWeightsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var deserializer = new BinaryFormatter();
            var dialog = new OpenFileDialog();
            dialog.DefaultExt = "dat";
            dialog.ShowDialog();

            using (Stream stream = File.Open(dialog.FileName, FileMode.Open))
            {
                _neuralNet = (NeuralNet) deserializer.Deserialize(stream);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

    public static class DictionaryExtensions
    {
        public static Dictionary<TKey, TValue> Shuffle<TKey, TValue>(
           this Dictionary<TKey, TValue> source)
        {
            Random r = new Random();
            return source.OrderBy(x => r.Next())
               .ToDictionary(item => item.Key, item => item.Value);
        }
    }
}
