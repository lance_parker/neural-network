﻿namespace Neural_Network
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbox00 = new System.Windows.Forms.PictureBox();
            this.pbox10 = new System.Windows.Forms.PictureBox();
            this.pbox30 = new System.Windows.Forms.PictureBox();
            this.pbox20 = new System.Windows.Forms.PictureBox();
            this.pbox70 = new System.Windows.Forms.PictureBox();
            this.pbox60 = new System.Windows.Forms.PictureBox();
            this.pbox50 = new System.Windows.Forms.PictureBox();
            this.pbox40 = new System.Windows.Forms.PictureBox();
            this.pbox71 = new System.Windows.Forms.PictureBox();
            this.pbox61 = new System.Windows.Forms.PictureBox();
            this.pbox51 = new System.Windows.Forms.PictureBox();
            this.pbox41 = new System.Windows.Forms.PictureBox();
            this.pbox31 = new System.Windows.Forms.PictureBox();
            this.pbox21 = new System.Windows.Forms.PictureBox();
            this.pbox11 = new System.Windows.Forms.PictureBox();
            this.pbox01 = new System.Windows.Forms.PictureBox();
            this.pbox72 = new System.Windows.Forms.PictureBox();
            this.pbox62 = new System.Windows.Forms.PictureBox();
            this.pbox52 = new System.Windows.Forms.PictureBox();
            this.pbox42 = new System.Windows.Forms.PictureBox();
            this.pbox32 = new System.Windows.Forms.PictureBox();
            this.pbox22 = new System.Windows.Forms.PictureBox();
            this.pbox12 = new System.Windows.Forms.PictureBox();
            this.pbox02 = new System.Windows.Forms.PictureBox();
            this.pbox75 = new System.Windows.Forms.PictureBox();
            this.pbox65 = new System.Windows.Forms.PictureBox();
            this.pbox55 = new System.Windows.Forms.PictureBox();
            this.pbox45 = new System.Windows.Forms.PictureBox();
            this.pbox35 = new System.Windows.Forms.PictureBox();
            this.pbox25 = new System.Windows.Forms.PictureBox();
            this.pbox15 = new System.Windows.Forms.PictureBox();
            this.pbox05 = new System.Windows.Forms.PictureBox();
            this.pbox74 = new System.Windows.Forms.PictureBox();
            this.pbox64 = new System.Windows.Forms.PictureBox();
            this.pbox54 = new System.Windows.Forms.PictureBox();
            this.pbox44 = new System.Windows.Forms.PictureBox();
            this.pbox34 = new System.Windows.Forms.PictureBox();
            this.pbox24 = new System.Windows.Forms.PictureBox();
            this.pbox14 = new System.Windows.Forms.PictureBox();
            this.pbox04 = new System.Windows.Forms.PictureBox();
            this.pbox73 = new System.Windows.Forms.PictureBox();
            this.pbox63 = new System.Windows.Forms.PictureBox();
            this.pbox53 = new System.Windows.Forms.PictureBox();
            this.pbox43 = new System.Windows.Forms.PictureBox();
            this.pbox33 = new System.Windows.Forms.PictureBox();
            this.pbox23 = new System.Windows.Forms.PictureBox();
            this.pbox13 = new System.Windows.Forms.PictureBox();
            this.pbox03 = new System.Windows.Forms.PictureBox();
            this.pbox78 = new System.Windows.Forms.PictureBox();
            this.pbox68 = new System.Windows.Forms.PictureBox();
            this.pbox58 = new System.Windows.Forms.PictureBox();
            this.pbox48 = new System.Windows.Forms.PictureBox();
            this.pbox38 = new System.Windows.Forms.PictureBox();
            this.pbox28 = new System.Windows.Forms.PictureBox();
            this.pbox18 = new System.Windows.Forms.PictureBox();
            this.pbox08 = new System.Windows.Forms.PictureBox();
            this.pbox77 = new System.Windows.Forms.PictureBox();
            this.pbox67 = new System.Windows.Forms.PictureBox();
            this.pbox57 = new System.Windows.Forms.PictureBox();
            this.pbox47 = new System.Windows.Forms.PictureBox();
            this.pbox37 = new System.Windows.Forms.PictureBox();
            this.pbox27 = new System.Windows.Forms.PictureBox();
            this.pbox17 = new System.Windows.Forms.PictureBox();
            this.pbox07 = new System.Windows.Forms.PictureBox();
            this.pbox76 = new System.Windows.Forms.PictureBox();
            this.pbox66 = new System.Windows.Forms.PictureBox();
            this.pbox56 = new System.Windows.Forms.PictureBox();
            this.pbox46 = new System.Windows.Forms.PictureBox();
            this.pbox36 = new System.Windows.Forms.PictureBox();
            this.pbox26 = new System.Windows.Forms.PictureBox();
            this.pbox16 = new System.Windows.Forms.PictureBox();
            this.pbox06 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnReadLetter = new System.Windows.Forms.Button();
            this.btnTrain = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveWeightsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadWeightsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pbox00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox07)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox06)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbox00
            // 
            this.pbox00.BackColor = System.Drawing.Color.White;
            this.pbox00.Location = new System.Drawing.Point(12, 39);
            this.pbox00.Name = "pbox00";
            this.pbox00.Size = new System.Drawing.Size(75, 75);
            this.pbox00.TabIndex = 1;
            this.pbox00.TabStop = false;
            this.pbox00.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox00.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox10
            // 
            this.pbox10.BackColor = System.Drawing.Color.White;
            this.pbox10.Location = new System.Drawing.Point(93, 39);
            this.pbox10.Name = "pbox10";
            this.pbox10.Size = new System.Drawing.Size(75, 75);
            this.pbox10.TabIndex = 2;
            this.pbox10.TabStop = false;
            this.pbox10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox10.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox30
            // 
            this.pbox30.BackColor = System.Drawing.Color.White;
            this.pbox30.Location = new System.Drawing.Point(255, 39);
            this.pbox30.Name = "pbox30";
            this.pbox30.Size = new System.Drawing.Size(75, 75);
            this.pbox30.TabIndex = 4;
            this.pbox30.TabStop = false;
            this.pbox30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox30.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox20
            // 
            this.pbox20.BackColor = System.Drawing.Color.White;
            this.pbox20.Location = new System.Drawing.Point(174, 39);
            this.pbox20.Name = "pbox20";
            this.pbox20.Size = new System.Drawing.Size(75, 75);
            this.pbox20.TabIndex = 3;
            this.pbox20.TabStop = false;
            this.pbox20.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox20.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox70
            // 
            this.pbox70.BackColor = System.Drawing.Color.White;
            this.pbox70.Location = new System.Drawing.Point(579, 39);
            this.pbox70.Name = "pbox70";
            this.pbox70.Size = new System.Drawing.Size(75, 75);
            this.pbox70.TabIndex = 8;
            this.pbox70.TabStop = false;
            this.pbox70.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox70.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox60
            // 
            this.pbox60.BackColor = System.Drawing.Color.White;
            this.pbox60.Location = new System.Drawing.Point(498, 39);
            this.pbox60.Name = "pbox60";
            this.pbox60.Size = new System.Drawing.Size(75, 75);
            this.pbox60.TabIndex = 7;
            this.pbox60.TabStop = false;
            this.pbox60.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox60.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox50
            // 
            this.pbox50.BackColor = System.Drawing.Color.White;
            this.pbox50.Location = new System.Drawing.Point(417, 39);
            this.pbox50.Name = "pbox50";
            this.pbox50.Size = new System.Drawing.Size(75, 75);
            this.pbox50.TabIndex = 6;
            this.pbox50.TabStop = false;
            this.pbox50.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox50.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox40
            // 
            this.pbox40.BackColor = System.Drawing.Color.White;
            this.pbox40.Location = new System.Drawing.Point(336, 39);
            this.pbox40.Name = "pbox40";
            this.pbox40.Size = new System.Drawing.Size(75, 75);
            this.pbox40.TabIndex = 5;
            this.pbox40.TabStop = false;
            this.pbox40.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox40.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox71
            // 
            this.pbox71.BackColor = System.Drawing.Color.White;
            this.pbox71.Location = new System.Drawing.Point(579, 120);
            this.pbox71.Name = "pbox71";
            this.pbox71.Size = new System.Drawing.Size(75, 75);
            this.pbox71.TabIndex = 17;
            this.pbox71.TabStop = false;
            this.pbox71.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox71.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox61
            // 
            this.pbox61.BackColor = System.Drawing.Color.White;
            this.pbox61.Location = new System.Drawing.Point(498, 120);
            this.pbox61.Name = "pbox61";
            this.pbox61.Size = new System.Drawing.Size(75, 75);
            this.pbox61.TabIndex = 16;
            this.pbox61.TabStop = false;
            this.pbox61.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox61.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox51
            // 
            this.pbox51.BackColor = System.Drawing.Color.White;
            this.pbox51.Location = new System.Drawing.Point(417, 120);
            this.pbox51.Name = "pbox51";
            this.pbox51.Size = new System.Drawing.Size(75, 75);
            this.pbox51.TabIndex = 15;
            this.pbox51.TabStop = false;
            this.pbox51.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox51.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox41
            // 
            this.pbox41.BackColor = System.Drawing.Color.White;
            this.pbox41.Location = new System.Drawing.Point(336, 120);
            this.pbox41.Name = "pbox41";
            this.pbox41.Size = new System.Drawing.Size(75, 75);
            this.pbox41.TabIndex = 14;
            this.pbox41.TabStop = false;
            this.pbox41.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox41.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox31
            // 
            this.pbox31.BackColor = System.Drawing.Color.White;
            this.pbox31.Location = new System.Drawing.Point(255, 120);
            this.pbox31.Name = "pbox31";
            this.pbox31.Size = new System.Drawing.Size(75, 75);
            this.pbox31.TabIndex = 13;
            this.pbox31.TabStop = false;
            this.pbox31.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox31.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox21
            // 
            this.pbox21.BackColor = System.Drawing.Color.White;
            this.pbox21.Location = new System.Drawing.Point(174, 120);
            this.pbox21.Name = "pbox21";
            this.pbox21.Size = new System.Drawing.Size(75, 75);
            this.pbox21.TabIndex = 12;
            this.pbox21.TabStop = false;
            this.pbox21.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox21.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox11
            // 
            this.pbox11.BackColor = System.Drawing.Color.White;
            this.pbox11.Location = new System.Drawing.Point(93, 120);
            this.pbox11.Name = "pbox11";
            this.pbox11.Size = new System.Drawing.Size(75, 75);
            this.pbox11.TabIndex = 11;
            this.pbox11.TabStop = false;
            this.pbox11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox11.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox01
            // 
            this.pbox01.BackColor = System.Drawing.Color.White;
            this.pbox01.Location = new System.Drawing.Point(12, 120);
            this.pbox01.Name = "pbox01";
            this.pbox01.Size = new System.Drawing.Size(75, 75);
            this.pbox01.TabIndex = 10;
            this.pbox01.TabStop = false;
            this.pbox01.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox01.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox72
            // 
            this.pbox72.BackColor = System.Drawing.Color.White;
            this.pbox72.Location = new System.Drawing.Point(579, 201);
            this.pbox72.Name = "pbox72";
            this.pbox72.Size = new System.Drawing.Size(75, 75);
            this.pbox72.TabIndex = 26;
            this.pbox72.TabStop = false;
            this.pbox72.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox72.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox62
            // 
            this.pbox62.BackColor = System.Drawing.Color.White;
            this.pbox62.Location = new System.Drawing.Point(498, 201);
            this.pbox62.Name = "pbox62";
            this.pbox62.Size = new System.Drawing.Size(75, 75);
            this.pbox62.TabIndex = 25;
            this.pbox62.TabStop = false;
            this.pbox62.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox62.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox52
            // 
            this.pbox52.BackColor = System.Drawing.Color.White;
            this.pbox52.Location = new System.Drawing.Point(417, 201);
            this.pbox52.Name = "pbox52";
            this.pbox52.Size = new System.Drawing.Size(75, 75);
            this.pbox52.TabIndex = 24;
            this.pbox52.TabStop = false;
            this.pbox52.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox52.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox42
            // 
            this.pbox42.BackColor = System.Drawing.Color.White;
            this.pbox42.Location = new System.Drawing.Point(336, 201);
            this.pbox42.Name = "pbox42";
            this.pbox42.Size = new System.Drawing.Size(75, 75);
            this.pbox42.TabIndex = 23;
            this.pbox42.TabStop = false;
            this.pbox42.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox42.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox32
            // 
            this.pbox32.BackColor = System.Drawing.Color.White;
            this.pbox32.Location = new System.Drawing.Point(255, 201);
            this.pbox32.Name = "pbox32";
            this.pbox32.Size = new System.Drawing.Size(75, 75);
            this.pbox32.TabIndex = 22;
            this.pbox32.TabStop = false;
            this.pbox32.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox32.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox22
            // 
            this.pbox22.BackColor = System.Drawing.Color.White;
            this.pbox22.Location = new System.Drawing.Point(174, 201);
            this.pbox22.Name = "pbox22";
            this.pbox22.Size = new System.Drawing.Size(75, 75);
            this.pbox22.TabIndex = 21;
            this.pbox22.TabStop = false;
            this.pbox22.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox22.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox12
            // 
            this.pbox12.BackColor = System.Drawing.Color.White;
            this.pbox12.Location = new System.Drawing.Point(93, 201);
            this.pbox12.Name = "pbox12";
            this.pbox12.Size = new System.Drawing.Size(75, 75);
            this.pbox12.TabIndex = 20;
            this.pbox12.TabStop = false;
            this.pbox12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox12.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox02
            // 
            this.pbox02.BackColor = System.Drawing.Color.White;
            this.pbox02.Location = new System.Drawing.Point(12, 201);
            this.pbox02.Name = "pbox02";
            this.pbox02.Size = new System.Drawing.Size(75, 75);
            this.pbox02.TabIndex = 19;
            this.pbox02.TabStop = false;
            this.pbox02.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox02.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox75
            // 
            this.pbox75.BackColor = System.Drawing.Color.White;
            this.pbox75.Location = new System.Drawing.Point(579, 444);
            this.pbox75.Name = "pbox75";
            this.pbox75.Size = new System.Drawing.Size(75, 75);
            this.pbox75.TabIndex = 53;
            this.pbox75.TabStop = false;
            this.pbox75.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox75.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox65
            // 
            this.pbox65.BackColor = System.Drawing.Color.White;
            this.pbox65.Location = new System.Drawing.Point(498, 444);
            this.pbox65.Name = "pbox65";
            this.pbox65.Size = new System.Drawing.Size(75, 75);
            this.pbox65.TabIndex = 52;
            this.pbox65.TabStop = false;
            this.pbox65.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox65.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox55
            // 
            this.pbox55.BackColor = System.Drawing.Color.White;
            this.pbox55.Location = new System.Drawing.Point(417, 444);
            this.pbox55.Name = "pbox55";
            this.pbox55.Size = new System.Drawing.Size(75, 75);
            this.pbox55.TabIndex = 51;
            this.pbox55.TabStop = false;
            this.pbox55.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox55.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox45
            // 
            this.pbox45.BackColor = System.Drawing.Color.White;
            this.pbox45.Location = new System.Drawing.Point(336, 444);
            this.pbox45.Name = "pbox45";
            this.pbox45.Size = new System.Drawing.Size(75, 75);
            this.pbox45.TabIndex = 50;
            this.pbox45.TabStop = false;
            this.pbox45.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox45.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox35
            // 
            this.pbox35.BackColor = System.Drawing.Color.White;
            this.pbox35.Location = new System.Drawing.Point(255, 444);
            this.pbox35.Name = "pbox35";
            this.pbox35.Size = new System.Drawing.Size(75, 75);
            this.pbox35.TabIndex = 49;
            this.pbox35.TabStop = false;
            this.pbox35.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox35.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox25
            // 
            this.pbox25.BackColor = System.Drawing.Color.White;
            this.pbox25.Location = new System.Drawing.Point(174, 444);
            this.pbox25.Name = "pbox25";
            this.pbox25.Size = new System.Drawing.Size(75, 75);
            this.pbox25.TabIndex = 48;
            this.pbox25.TabStop = false;
            this.pbox25.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox25.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox15
            // 
            this.pbox15.BackColor = System.Drawing.Color.White;
            this.pbox15.Location = new System.Drawing.Point(93, 444);
            this.pbox15.Name = "pbox15";
            this.pbox15.Size = new System.Drawing.Size(75, 75);
            this.pbox15.TabIndex = 47;
            this.pbox15.TabStop = false;
            this.pbox15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox15.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox05
            // 
            this.pbox05.BackColor = System.Drawing.Color.White;
            this.pbox05.Location = new System.Drawing.Point(12, 444);
            this.pbox05.Name = "pbox05";
            this.pbox05.Size = new System.Drawing.Size(75, 75);
            this.pbox05.TabIndex = 46;
            this.pbox05.TabStop = false;
            this.pbox05.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox05.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox74
            // 
            this.pbox74.BackColor = System.Drawing.Color.White;
            this.pbox74.Location = new System.Drawing.Point(579, 363);
            this.pbox74.Name = "pbox74";
            this.pbox74.Size = new System.Drawing.Size(75, 75);
            this.pbox74.TabIndex = 44;
            this.pbox74.TabStop = false;
            this.pbox74.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox74.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox64
            // 
            this.pbox64.BackColor = System.Drawing.Color.White;
            this.pbox64.Location = new System.Drawing.Point(498, 363);
            this.pbox64.Name = "pbox64";
            this.pbox64.Size = new System.Drawing.Size(75, 75);
            this.pbox64.TabIndex = 43;
            this.pbox64.TabStop = false;
            this.pbox64.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox64.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox54
            // 
            this.pbox54.BackColor = System.Drawing.Color.White;
            this.pbox54.Location = new System.Drawing.Point(417, 363);
            this.pbox54.Name = "pbox54";
            this.pbox54.Size = new System.Drawing.Size(75, 75);
            this.pbox54.TabIndex = 42;
            this.pbox54.TabStop = false;
            this.pbox54.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox54.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox44
            // 
            this.pbox44.BackColor = System.Drawing.Color.White;
            this.pbox44.Location = new System.Drawing.Point(336, 363);
            this.pbox44.Name = "pbox44";
            this.pbox44.Size = new System.Drawing.Size(75, 75);
            this.pbox44.TabIndex = 41;
            this.pbox44.TabStop = false;
            this.pbox44.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox44.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox34
            // 
            this.pbox34.BackColor = System.Drawing.Color.White;
            this.pbox34.Location = new System.Drawing.Point(255, 363);
            this.pbox34.Name = "pbox34";
            this.pbox34.Size = new System.Drawing.Size(75, 75);
            this.pbox34.TabIndex = 40;
            this.pbox34.TabStop = false;
            this.pbox34.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox34.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox24
            // 
            this.pbox24.BackColor = System.Drawing.Color.White;
            this.pbox24.Location = new System.Drawing.Point(174, 363);
            this.pbox24.Name = "pbox24";
            this.pbox24.Size = new System.Drawing.Size(75, 75);
            this.pbox24.TabIndex = 39;
            this.pbox24.TabStop = false;
            this.pbox24.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox24.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox14
            // 
            this.pbox14.BackColor = System.Drawing.Color.White;
            this.pbox14.Location = new System.Drawing.Point(93, 363);
            this.pbox14.Name = "pbox14";
            this.pbox14.Size = new System.Drawing.Size(75, 75);
            this.pbox14.TabIndex = 38;
            this.pbox14.TabStop = false;
            this.pbox14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox14.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox04
            // 
            this.pbox04.BackColor = System.Drawing.Color.White;
            this.pbox04.Location = new System.Drawing.Point(12, 363);
            this.pbox04.Name = "pbox04";
            this.pbox04.Size = new System.Drawing.Size(75, 75);
            this.pbox04.TabIndex = 37;
            this.pbox04.TabStop = false;
            this.pbox04.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox04.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox73
            // 
            this.pbox73.BackColor = System.Drawing.Color.White;
            this.pbox73.Location = new System.Drawing.Point(579, 282);
            this.pbox73.Name = "pbox73";
            this.pbox73.Size = new System.Drawing.Size(75, 75);
            this.pbox73.TabIndex = 35;
            this.pbox73.TabStop = false;
            this.pbox73.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox73.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox63
            // 
            this.pbox63.BackColor = System.Drawing.Color.White;
            this.pbox63.Location = new System.Drawing.Point(498, 282);
            this.pbox63.Name = "pbox63";
            this.pbox63.Size = new System.Drawing.Size(75, 75);
            this.pbox63.TabIndex = 34;
            this.pbox63.TabStop = false;
            this.pbox63.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox63.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox53
            // 
            this.pbox53.BackColor = System.Drawing.Color.White;
            this.pbox53.Location = new System.Drawing.Point(417, 282);
            this.pbox53.Name = "pbox53";
            this.pbox53.Size = new System.Drawing.Size(75, 75);
            this.pbox53.TabIndex = 33;
            this.pbox53.TabStop = false;
            this.pbox53.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox53.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox43
            // 
            this.pbox43.BackColor = System.Drawing.Color.White;
            this.pbox43.Location = new System.Drawing.Point(336, 282);
            this.pbox43.Name = "pbox43";
            this.pbox43.Size = new System.Drawing.Size(75, 75);
            this.pbox43.TabIndex = 32;
            this.pbox43.TabStop = false;
            this.pbox43.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox43.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox33
            // 
            this.pbox33.BackColor = System.Drawing.Color.White;
            this.pbox33.Location = new System.Drawing.Point(255, 282);
            this.pbox33.Name = "pbox33";
            this.pbox33.Size = new System.Drawing.Size(75, 75);
            this.pbox33.TabIndex = 31;
            this.pbox33.TabStop = false;
            this.pbox33.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox33.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox23
            // 
            this.pbox23.BackColor = System.Drawing.Color.White;
            this.pbox23.Location = new System.Drawing.Point(174, 282);
            this.pbox23.Name = "pbox23";
            this.pbox23.Size = new System.Drawing.Size(75, 75);
            this.pbox23.TabIndex = 30;
            this.pbox23.TabStop = false;
            this.pbox23.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox23.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox13
            // 
            this.pbox13.BackColor = System.Drawing.Color.White;
            this.pbox13.Location = new System.Drawing.Point(93, 282);
            this.pbox13.Name = "pbox13";
            this.pbox13.Size = new System.Drawing.Size(75, 75);
            this.pbox13.TabIndex = 29;
            this.pbox13.TabStop = false;
            this.pbox13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox13.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox03
            // 
            this.pbox03.BackColor = System.Drawing.Color.White;
            this.pbox03.Location = new System.Drawing.Point(12, 282);
            this.pbox03.Name = "pbox03";
            this.pbox03.Size = new System.Drawing.Size(75, 75);
            this.pbox03.TabIndex = 28;
            this.pbox03.TabStop = false;
            this.pbox03.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox03.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox78
            // 
            this.pbox78.BackColor = System.Drawing.Color.White;
            this.pbox78.Location = new System.Drawing.Point(579, 687);
            this.pbox78.Name = "pbox78";
            this.pbox78.Size = new System.Drawing.Size(75, 75);
            this.pbox78.TabIndex = 80;
            this.pbox78.TabStop = false;
            this.pbox78.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox78.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox68
            // 
            this.pbox68.BackColor = System.Drawing.Color.White;
            this.pbox68.Location = new System.Drawing.Point(498, 687);
            this.pbox68.Name = "pbox68";
            this.pbox68.Size = new System.Drawing.Size(75, 75);
            this.pbox68.TabIndex = 79;
            this.pbox68.TabStop = false;
            this.pbox68.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox68.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox58
            // 
            this.pbox58.BackColor = System.Drawing.Color.White;
            this.pbox58.Location = new System.Drawing.Point(417, 687);
            this.pbox58.Name = "pbox58";
            this.pbox58.Size = new System.Drawing.Size(75, 75);
            this.pbox58.TabIndex = 78;
            this.pbox58.TabStop = false;
            this.pbox58.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox58.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox48
            // 
            this.pbox48.BackColor = System.Drawing.Color.White;
            this.pbox48.Location = new System.Drawing.Point(336, 687);
            this.pbox48.Name = "pbox48";
            this.pbox48.Size = new System.Drawing.Size(75, 75);
            this.pbox48.TabIndex = 77;
            this.pbox48.TabStop = false;
            this.pbox48.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox48.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox38
            // 
            this.pbox38.BackColor = System.Drawing.Color.White;
            this.pbox38.Location = new System.Drawing.Point(255, 687);
            this.pbox38.Name = "pbox38";
            this.pbox38.Size = new System.Drawing.Size(75, 75);
            this.pbox38.TabIndex = 76;
            this.pbox38.TabStop = false;
            this.pbox38.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox38.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox28
            // 
            this.pbox28.BackColor = System.Drawing.Color.White;
            this.pbox28.Location = new System.Drawing.Point(174, 687);
            this.pbox28.Name = "pbox28";
            this.pbox28.Size = new System.Drawing.Size(75, 75);
            this.pbox28.TabIndex = 75;
            this.pbox28.TabStop = false;
            this.pbox28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox28.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox18
            // 
            this.pbox18.BackColor = System.Drawing.Color.White;
            this.pbox18.Location = new System.Drawing.Point(93, 687);
            this.pbox18.Name = "pbox18";
            this.pbox18.Size = new System.Drawing.Size(75, 75);
            this.pbox18.TabIndex = 74;
            this.pbox18.TabStop = false;
            this.pbox18.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox18.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox08
            // 
            this.pbox08.BackColor = System.Drawing.Color.White;
            this.pbox08.Location = new System.Drawing.Point(12, 687);
            this.pbox08.Name = "pbox08";
            this.pbox08.Size = new System.Drawing.Size(75, 75);
            this.pbox08.TabIndex = 73;
            this.pbox08.TabStop = false;
            this.pbox08.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox08.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox77
            // 
            this.pbox77.BackColor = System.Drawing.Color.White;
            this.pbox77.Location = new System.Drawing.Point(579, 606);
            this.pbox77.Name = "pbox77";
            this.pbox77.Size = new System.Drawing.Size(75, 75);
            this.pbox77.TabIndex = 71;
            this.pbox77.TabStop = false;
            this.pbox77.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox77.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox67
            // 
            this.pbox67.BackColor = System.Drawing.Color.White;
            this.pbox67.Location = new System.Drawing.Point(498, 606);
            this.pbox67.Name = "pbox67";
            this.pbox67.Size = new System.Drawing.Size(75, 75);
            this.pbox67.TabIndex = 70;
            this.pbox67.TabStop = false;
            this.pbox67.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox67.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox57
            // 
            this.pbox57.BackColor = System.Drawing.Color.White;
            this.pbox57.Location = new System.Drawing.Point(417, 606);
            this.pbox57.Name = "pbox57";
            this.pbox57.Size = new System.Drawing.Size(75, 75);
            this.pbox57.TabIndex = 69;
            this.pbox57.TabStop = false;
            this.pbox57.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox57.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox47
            // 
            this.pbox47.BackColor = System.Drawing.Color.White;
            this.pbox47.Location = new System.Drawing.Point(336, 606);
            this.pbox47.Name = "pbox47";
            this.pbox47.Size = new System.Drawing.Size(75, 75);
            this.pbox47.TabIndex = 68;
            this.pbox47.TabStop = false;
            this.pbox47.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox47.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox37
            // 
            this.pbox37.BackColor = System.Drawing.Color.White;
            this.pbox37.Location = new System.Drawing.Point(255, 606);
            this.pbox37.Name = "pbox37";
            this.pbox37.Size = new System.Drawing.Size(75, 75);
            this.pbox37.TabIndex = 67;
            this.pbox37.TabStop = false;
            this.pbox37.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox37.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox27
            // 
            this.pbox27.BackColor = System.Drawing.Color.White;
            this.pbox27.Location = new System.Drawing.Point(174, 606);
            this.pbox27.Name = "pbox27";
            this.pbox27.Size = new System.Drawing.Size(75, 75);
            this.pbox27.TabIndex = 66;
            this.pbox27.TabStop = false;
            this.pbox27.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox27.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox17
            // 
            this.pbox17.BackColor = System.Drawing.Color.White;
            this.pbox17.Location = new System.Drawing.Point(93, 606);
            this.pbox17.Name = "pbox17";
            this.pbox17.Size = new System.Drawing.Size(75, 75);
            this.pbox17.TabIndex = 65;
            this.pbox17.TabStop = false;
            this.pbox17.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox17.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox07
            // 
            this.pbox07.BackColor = System.Drawing.Color.White;
            this.pbox07.Location = new System.Drawing.Point(12, 606);
            this.pbox07.Name = "pbox07";
            this.pbox07.Size = new System.Drawing.Size(75, 75);
            this.pbox07.TabIndex = 64;
            this.pbox07.TabStop = false;
            this.pbox07.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox07.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox76
            // 
            this.pbox76.BackColor = System.Drawing.Color.White;
            this.pbox76.Location = new System.Drawing.Point(579, 525);
            this.pbox76.Name = "pbox76";
            this.pbox76.Size = new System.Drawing.Size(75, 75);
            this.pbox76.TabIndex = 62;
            this.pbox76.TabStop = false;
            this.pbox76.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox76.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox66
            // 
            this.pbox66.BackColor = System.Drawing.Color.White;
            this.pbox66.Location = new System.Drawing.Point(498, 525);
            this.pbox66.Name = "pbox66";
            this.pbox66.Size = new System.Drawing.Size(75, 75);
            this.pbox66.TabIndex = 61;
            this.pbox66.TabStop = false;
            this.pbox66.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox66.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox56
            // 
            this.pbox56.BackColor = System.Drawing.Color.White;
            this.pbox56.Location = new System.Drawing.Point(417, 525);
            this.pbox56.Name = "pbox56";
            this.pbox56.Size = new System.Drawing.Size(75, 75);
            this.pbox56.TabIndex = 60;
            this.pbox56.TabStop = false;
            this.pbox56.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox56.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox46
            // 
            this.pbox46.BackColor = System.Drawing.Color.White;
            this.pbox46.Location = new System.Drawing.Point(336, 525);
            this.pbox46.Name = "pbox46";
            this.pbox46.Size = new System.Drawing.Size(75, 75);
            this.pbox46.TabIndex = 59;
            this.pbox46.TabStop = false;
            this.pbox46.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox46.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox36
            // 
            this.pbox36.BackColor = System.Drawing.Color.White;
            this.pbox36.Location = new System.Drawing.Point(255, 525);
            this.pbox36.Name = "pbox36";
            this.pbox36.Size = new System.Drawing.Size(75, 75);
            this.pbox36.TabIndex = 58;
            this.pbox36.TabStop = false;
            this.pbox36.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox36.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox26
            // 
            this.pbox26.BackColor = System.Drawing.Color.White;
            this.pbox26.Location = new System.Drawing.Point(174, 525);
            this.pbox26.Name = "pbox26";
            this.pbox26.Size = new System.Drawing.Size(75, 75);
            this.pbox26.TabIndex = 57;
            this.pbox26.TabStop = false;
            this.pbox26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox26.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox16
            // 
            this.pbox16.BackColor = System.Drawing.Color.White;
            this.pbox16.Location = new System.Drawing.Point(93, 525);
            this.pbox16.Name = "pbox16";
            this.pbox16.Size = new System.Drawing.Size(75, 75);
            this.pbox16.TabIndex = 56;
            this.pbox16.TabStop = false;
            this.pbox16.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox16.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // pbox06
            // 
            this.pbox06.BackColor = System.Drawing.Color.White;
            this.pbox06.Location = new System.Drawing.Point(12, 525);
            this.pbox06.Name = "pbox06";
            this.pbox06.Size = new System.Drawing.Size(75, 75);
            this.pbox06.TabIndex = 55;
            this.pbox06.TabStop = false;
            this.pbox06.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickAnywhere);
            this.pbox06.MouseEnter += new System.EventHandler(this.MouseEnterImageBox);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 788);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 83;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ClearButtonClick);
            // 
            // btnReadLetter
            // 
            this.btnReadLetter.Location = new System.Drawing.Point(579, 788);
            this.btnReadLetter.Name = "btnReadLetter";
            this.btnReadLetter.Size = new System.Drawing.Size(75, 23);
            this.btnReadLetter.TabIndex = 84;
            this.btnReadLetter.Text = "Read Letter";
            this.btnReadLetter.UseVisualStyleBackColor = true;
            this.btnReadLetter.Click += new System.EventHandler(this.btnReadLetter_Click);
            // 
            // btnTrain
            // 
            this.btnTrain.Location = new System.Drawing.Point(498, 788);
            this.btnTrain.Name = "btnTrain";
            this.btnTrain.Size = new System.Drawing.Size(75, 23);
            this.btnTrain.TabIndex = 85;
            this.btnTrain.Text = "Train";
            this.btnTrain.UseVisualStyleBackColor = true;
            this.btnTrain.Click += new System.EventHandler(this.btnTrain_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(665, 24);
            this.menuStrip1.TabIndex = 86;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveWeightsToolStripMenuItem,
            this.loadWeightsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveWeightsToolStripMenuItem
            // 
            this.saveWeightsToolStripMenuItem.Name = "saveWeightsToolStripMenuItem";
            this.saveWeightsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveWeightsToolStripMenuItem.Text = "SaveWeights";
            this.saveWeightsToolStripMenuItem.Click += new System.EventHandler(this.saveWeightsToolStripMenuItem_Click);
            // 
            // loadWeightsToolStripMenuItem
            // 
            this.loadWeightsToolStripMenuItem.Name = "loadWeightsToolStripMenuItem";
            this.loadWeightsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadWeightsToolStripMenuItem.Text = "Load Weights";
            this.loadWeightsToolStripMenuItem.Click += new System.EventHandler(this.loadWeightsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 822);
            this.Controls.Add(this.btnTrain);
            this.Controls.Add(this.btnReadLetter);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pbox78);
            this.Controls.Add(this.pbox68);
            this.Controls.Add(this.pbox58);
            this.Controls.Add(this.pbox48);
            this.Controls.Add(this.pbox38);
            this.Controls.Add(this.pbox28);
            this.Controls.Add(this.pbox18);
            this.Controls.Add(this.pbox08);
            this.Controls.Add(this.pbox77);
            this.Controls.Add(this.pbox67);
            this.Controls.Add(this.pbox57);
            this.Controls.Add(this.pbox47);
            this.Controls.Add(this.pbox37);
            this.Controls.Add(this.pbox27);
            this.Controls.Add(this.pbox17);
            this.Controls.Add(this.pbox07);
            this.Controls.Add(this.pbox76);
            this.Controls.Add(this.pbox66);
            this.Controls.Add(this.pbox56);
            this.Controls.Add(this.pbox46);
            this.Controls.Add(this.pbox36);
            this.Controls.Add(this.pbox26);
            this.Controls.Add(this.pbox16);
            this.Controls.Add(this.pbox06);
            this.Controls.Add(this.pbox75);
            this.Controls.Add(this.pbox65);
            this.Controls.Add(this.pbox55);
            this.Controls.Add(this.pbox45);
            this.Controls.Add(this.pbox35);
            this.Controls.Add(this.pbox25);
            this.Controls.Add(this.pbox15);
            this.Controls.Add(this.pbox05);
            this.Controls.Add(this.pbox74);
            this.Controls.Add(this.pbox64);
            this.Controls.Add(this.pbox54);
            this.Controls.Add(this.pbox44);
            this.Controls.Add(this.pbox34);
            this.Controls.Add(this.pbox24);
            this.Controls.Add(this.pbox14);
            this.Controls.Add(this.pbox04);
            this.Controls.Add(this.pbox73);
            this.Controls.Add(this.pbox63);
            this.Controls.Add(this.pbox53);
            this.Controls.Add(this.pbox43);
            this.Controls.Add(this.pbox33);
            this.Controls.Add(this.pbox23);
            this.Controls.Add(this.pbox13);
            this.Controls.Add(this.pbox03);
            this.Controls.Add(this.pbox72);
            this.Controls.Add(this.pbox62);
            this.Controls.Add(this.pbox52);
            this.Controls.Add(this.pbox42);
            this.Controls.Add(this.pbox32);
            this.Controls.Add(this.pbox22);
            this.Controls.Add(this.pbox12);
            this.Controls.Add(this.pbox02);
            this.Controls.Add(this.pbox71);
            this.Controls.Add(this.pbox61);
            this.Controls.Add(this.pbox51);
            this.Controls.Add(this.pbox41);
            this.Controls.Add(this.pbox31);
            this.Controls.Add(this.pbox21);
            this.Controls.Add(this.pbox11);
            this.Controls.Add(this.pbox01);
            this.Controls.Add(this.pbox70);
            this.Controls.Add(this.pbox60);
            this.Controls.Add(this.pbox50);
            this.Controls.Add(this.pbox40);
            this.Controls.Add(this.pbox30);
            this.Controls.Add(this.pbox20);
            this.Controls.Add(this.pbox10);
            this.Controls.Add(this.pbox00);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Digit recognizer";
            this.Click += new System.EventHandler(this.MouseClickAnywhere);
            ((System.ComponentModel.ISupportInitialize)(this.pbox00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox07)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbox06)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbox00;
        private System.Windows.Forms.PictureBox pbox10;
        private System.Windows.Forms.PictureBox pbox30;
        private System.Windows.Forms.PictureBox pbox20;
        private System.Windows.Forms.PictureBox pbox70;
        private System.Windows.Forms.PictureBox pbox60;
        private System.Windows.Forms.PictureBox pbox50;
        private System.Windows.Forms.PictureBox pbox40;
        private System.Windows.Forms.PictureBox pbox71;
        private System.Windows.Forms.PictureBox pbox61;
        private System.Windows.Forms.PictureBox pbox51;
        private System.Windows.Forms.PictureBox pbox41;
        private System.Windows.Forms.PictureBox pbox31;
        private System.Windows.Forms.PictureBox pbox21;
        private System.Windows.Forms.PictureBox pbox11;
        private System.Windows.Forms.PictureBox pbox01;
        private System.Windows.Forms.PictureBox pbox72;
        private System.Windows.Forms.PictureBox pbox62;
        private System.Windows.Forms.PictureBox pbox52;
        private System.Windows.Forms.PictureBox pbox42;
        private System.Windows.Forms.PictureBox pbox32;
        private System.Windows.Forms.PictureBox pbox22;
        private System.Windows.Forms.PictureBox pbox12;
        private System.Windows.Forms.PictureBox pbox02;
        private System.Windows.Forms.PictureBox pbox75;
        private System.Windows.Forms.PictureBox pbox65;
        private System.Windows.Forms.PictureBox pbox55;
        private System.Windows.Forms.PictureBox pbox45;
        private System.Windows.Forms.PictureBox pbox35;
        private System.Windows.Forms.PictureBox pbox25;
        private System.Windows.Forms.PictureBox pbox15;
        private System.Windows.Forms.PictureBox pbox05;
        private System.Windows.Forms.PictureBox pbox74;
        private System.Windows.Forms.PictureBox pbox64;
        private System.Windows.Forms.PictureBox pbox54;
        private System.Windows.Forms.PictureBox pbox44;
        private System.Windows.Forms.PictureBox pbox34;
        private System.Windows.Forms.PictureBox pbox24;
        private System.Windows.Forms.PictureBox pbox14;
        private System.Windows.Forms.PictureBox pbox04;
        private System.Windows.Forms.PictureBox pbox73;
        private System.Windows.Forms.PictureBox pbox63;
        private System.Windows.Forms.PictureBox pbox53;
        private System.Windows.Forms.PictureBox pbox43;
        private System.Windows.Forms.PictureBox pbox33;
        private System.Windows.Forms.PictureBox pbox23;
        private System.Windows.Forms.PictureBox pbox13;
        private System.Windows.Forms.PictureBox pbox03;
        private System.Windows.Forms.PictureBox pbox78;
        private System.Windows.Forms.PictureBox pbox68;
        private System.Windows.Forms.PictureBox pbox58;
        private System.Windows.Forms.PictureBox pbox48;
        private System.Windows.Forms.PictureBox pbox38;
        private System.Windows.Forms.PictureBox pbox28;
        private System.Windows.Forms.PictureBox pbox18;
        private System.Windows.Forms.PictureBox pbox08;
        private System.Windows.Forms.PictureBox pbox77;
        private System.Windows.Forms.PictureBox pbox67;
        private System.Windows.Forms.PictureBox pbox57;
        private System.Windows.Forms.PictureBox pbox47;
        private System.Windows.Forms.PictureBox pbox37;
        private System.Windows.Forms.PictureBox pbox27;
        private System.Windows.Forms.PictureBox pbox17;
        private System.Windows.Forms.PictureBox pbox07;
        private System.Windows.Forms.PictureBox pbox76;
        private System.Windows.Forms.PictureBox pbox66;
        private System.Windows.Forms.PictureBox pbox56;
        private System.Windows.Forms.PictureBox pbox46;
        private System.Windows.Forms.PictureBox pbox36;
        private System.Windows.Forms.PictureBox pbox26;
        private System.Windows.Forms.PictureBox pbox16;
        private System.Windows.Forms.PictureBox pbox06;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnReadLetter;
        private System.Windows.Forms.Button btnTrain;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWeightsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadWeightsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

