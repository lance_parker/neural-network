﻿using System;
using System.Xml.Serialization;

namespace Neural_Network
{
    [Serializable]
    public class NeuralNet
    {
        private const int Inputs = 8*9;
        private const int Outputs = 5;
        private const int HiddenUnits = 25;
        private const double LearningRate = 0.05;
        private const double Momentum = 0.5;
        private readonly int[] _outputMeanings;

        //weights
        private double[,] _inputToHiddenWeights;
        private double[,] _hiddenToOutputWeights;
        private readonly double[,] _oldHiddenToOutputWeights;
        
        //hidden units
        private readonly double[] _hiddenUnitValues;

        //error
        public double SumError;

        public NeuralNet()
        {
            var rand = new Random();
            _outputMeanings = new[]{1,2,4,7,8};
            _inputToHiddenWeights = new double[(Inputs + 1), HiddenUnits + 1];
            _hiddenToOutputWeights = new double[(HiddenUnits + 1), Outputs];
            _oldHiddenToOutputWeights = new double[(HiddenUnits + 1), Outputs];

            _hiddenUnitValues = new double[HiddenUnits + 1];

            for (int i = 0; i < Inputs+1; i++)
            {
                for (int j = 0; j < HiddenUnits; j++)
                {
                    double val = rand.NextDouble() * (.05 - (-.05)) + (-.05);
                    _inputToHiddenWeights[i, j] = val;
                }
            }

            for (int i = 0; i < HiddenUnits+1; i++)
            {
                for (int j = 0; j < Outputs; j++)
                {
                    _hiddenToOutputWeights[i, j] = rand.NextDouble() * (.05 - (-.05)) + (-.05);
                }
            }
        }

        public double[] TestInput(ref double[] _in)
        {
            var input = new double[_in.Length + 1];
            input[0] = 1.0;
            for (int i = 0; i < _in.Length; i++)
            {
                input[i + 1] = _in[i];
            }
            
            var output = new double[Outputs];

            // feed forward
            Array.Copy(_hiddenToOutputWeights, _oldHiddenToOutputWeights, _hiddenToOutputWeights.Length);
            _hiddenUnitValues[0] = 1.0;
            for (int j = 1; j < _hiddenUnitValues.Length; j++)
            {
                double sum = 0.0;
                for (int i = 0; i < input.Length; i++)
                {
                    sum += _inputToHiddenWeights[i, j] * input[i];
                }
                _hiddenUnitValues[j] = SigmoidValue(sum);
            }

            for (int k = 0; k < output.Length; k++)
            {
                double sum = 0.0;
                for (int j = 0; j < _hiddenUnitValues.Length; j++)
                {
                    sum += _hiddenToOutputWeights[j, k] * _hiddenUnitValues[j];
                }
                output[k] = SigmoidValue(sum);
            }
            return output;
        }

        public double[] Input(double[] input, int expectedResult)
        {
            double[] expectedOutputs = ExpectedOutputs(expectedResult);
            double[] output = TestInput(ref input);
            // back prop
            // hidden units to outputs
            SumError = 0.0;
            for (int j = 0; j < _hiddenUnitValues.Length; j++)
            {
                for (int i = 0; i < expectedOutputs.Length; i++)
                {
                    double error = (output[i]*(1 - output[i]))*(expectedOutputs[i] - output[i]);
                    double weightChange = LearningRate * error * _hiddenUnitValues[j];
                    _hiddenToOutputWeights[j, i] += weightChange;
                    SumError += error;
                }
            }

            // inputs to hidden units
            for (int j = 0; j < input.Length; j++)
            {
                for (int i = 0; i < _hiddenUnitValues.Length; i++)
                {
                    double sum = 0.0;
                    for (int k = 0; k < output.Length; k++)
                    {
                        double outputError = output[k]*(1 - output[k])*(expectedOutputs[k] - output[k]);
                        sum += outputError*_oldHiddenToOutputWeights[i, k];
                    }
                    double error = _hiddenUnitValues[i]*(1 - _hiddenUnitValues[i]) * sum;
                    double weightChange = LearningRate * error * input[j];
                    _inputToHiddenWeights[j, i] += weightChange;
                }
            }
            return output;
        }

        private double SigmoidValue(double value)
        {
            return 1 / (1 + Math.Pow(Math.E,-value));
        }

        private double[] ExpectedOutputs(int value)
        {
            var result = new[] {0.1, 0.1, 0.1, 0.1, 0.1};
            for (int i = 0; i < _outputMeanings.Length; i++)
            {
                if (_outputMeanings[i] == value)
                {
                    result[i] = 0.9;
                }
            }
            return result;
        }

        public int HighestValue(double[] results)
        {
            int highIndex = 0;
            double highValue = results[highIndex];
            for (int i = 0; i < results.Length; i++)
            {
                if (highValue < results[i])
                {
                    highIndex = i;
                    highValue = results[i];
                }
            }
            return _outputMeanings[highIndex];
        }

        public string InterperetResults(double[] results)
        {
            int highIndex = 0;
            double highValue = results[highIndex];
            string output = "";
            for (int i = 0; i < results.Length; i++)
            {
                output += _outputMeanings[i] + ": " + results[i] + "\n"; 
                if (highValue < results[i])
                {
                    highIndex = i;
                    highValue = results[i];
                }
            }
             output = "You drew a(n) " + _outputMeanings[highIndex] + "\n" + output;
            return output;
        }
    }
}
